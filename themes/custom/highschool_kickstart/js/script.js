(function($, Drupal){
  Drupal.behaviors.globalBehavior = {
    attach: function(context, settings) {
      if ($('.block-alert-banner .messages__button', context).length > 0) {
        $('.block-alert-banner .messages__button', context).click(function(){
          $(this).parents('.block-alert-banner').hide();
        });
      }
    }
  }
})(jQuery, Drupal);
