module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    sass: {
      dist: {
        options: {
          sourcemap: 'none',
          style: 'compressed',
        },
        files: [{
          expand: true,
          cwd: 'css/sass/',
          src: ['*.scss'],
          dest: 'css/',
          ext: '.css'
        }]
      },
    },
    uglify: {
      options: {
        mangle: {
          reserved: ['jQuery', 'Drupal', 'drupalSettings', '$']
        }
      },
      my_target: {
        files: [{
          expand: true,
          cwd: 'js/',
          src: ['*.js'],
          dest: 'js/min/',
          ext: '.min.js'
        }]
      }
    },
    watch: {
      css: {
        files: 'css/sass/*.scss',
        tasks: ['sass']
      },
      js: {
        files: 'js/*.js',
        tasks: ['uglify']
      }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.registerTask('default',['watch']);
}
