
CONTENTS OF THIS FILE
---------------------

 * About highschool_kickstart theme
 * How to use grunt

About highschool_kickstart theme
----------------------

Custom theme built for highschool_kickstart profile which is been extended from core
olivero theme. This theme has following regions:
 - Header
 - Primary menu
 - Secondary menu
 - Hero (full width)
 - Highlighted
 - Breadcrumb
 - Social Bar
 - Content Above
 - Content
 - Sidebar
 - Content Below
 - Footer Top
 - Footer Bottom

How to use grunt
----------------

Prerequisite: Make sure following are installed.
 - npm (https://www.npmjs.com/get-npm)
 - ruby (https://www.ruby-lang.org/en/documentation/installation/)
 - sass (https://sass-lang.com/install)
 - grunt-cli (https://www.npmjs.com/package/grunt-cli)

Installing grunt:
 1. Navigate to highschool_kickstart theme root directory.
 2. Run command `npm install`

Grunt commands:
 1. `grunt sass` to compile all sass files and generate corresponding css files.
 2. `grunt uglify` to minify javascript files.
 3. `grunt` or `grunt watch` to compile sass and minify js whenever files are
added, changed or deleted
